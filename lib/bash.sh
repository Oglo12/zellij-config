#!/usr/bin/env bash

alias zellij-update='~/.config/zellij/update.sh'
alias zellij-change-theme='~/.config/zellij/change_theme.sh'
