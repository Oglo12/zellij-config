#!/usr/bin/env bash

skipline () {
	echo " "
}

skipline

cd ~/.config/zellij
git pull origin main

skipline

echo -e "  \033[1;30m[\033[1;32mi\033[1;30m]\033[0m Done."

skipline
