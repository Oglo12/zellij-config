#!/usr/bin/env bash

chosen_theme=$(ls ~/.config/zellij/themes | fzf --height=~15% --border=double)

rm ~/.config/zellij/config.kdl > /dev/null 2>&1
cat ~/.config/zellij/template.kdl | sed "s/--theme--/$chosen_theme/g" > ~/.config/zellij/config.kdl
